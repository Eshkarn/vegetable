/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author eshkarn
 */
import java.util.ArrayList;
import java.util.List;


public class Test {
    public static void main(String[] args) {
        Beet b1=new Beet("red",2);
        Beet b2=new Beet("Purple",1);
        
        Carrot c1=new Carrot("Orange", 1.5);
        Carrot c2=new Carrot("red", 2.5);
        
        List<Vegetables> veg =new ArrayList<Vegetables>();
        veg.add(b1);
        veg.add(b2);
        veg.add(c1);
        veg.add(c2);
        
        System.out.println("Beet 1: " + b1.getColor() +" "+ b1.getSize());
        b1.isRipe();
        System.out.println("Beet2 : " + b2.getColor()+ " " + b2.getSize());
         b2.isRipe();
        System.out.println("Carrot 1: " + c1.getColor() + " " + c1.getSize());
         c1.isRipe();
        System.out.println("Carrot 2: " + c2.getColor() + " " + c2.getSize());
         c2.isRipe();
        
        
    }
    
}
