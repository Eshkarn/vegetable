/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author eshkarn
 */

public abstract class Vegetables {
    protected String color;
    protected double size;

    public Vegetables(String color, double size) {
        this.color = color;
        this.size = size;
    }

    public abstract String getColor();

    public abstract double getSize() ;
    
    
    
}
