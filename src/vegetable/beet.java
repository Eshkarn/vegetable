/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author eshka
 */
public class Beet extends Vegetables{

    public Beet(String color, double size) {
        super(color, size);
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public double getSize() {
        return this.size;
    }
    
    public void isRipe(){
    if("red".equals(this.color) || this.size==2){
        System.out.println("Beet is riped");
    
    }
    else{
        System.out.println("Not riped");
    }
    
    }
}
