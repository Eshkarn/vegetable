/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

enum Type {
   BEET, CARROT;
}
public class VegetableFactory {
     private static VegetableFactory vegtableFactory = null;

   private VegetableFactory() {

   }

   public static VegetableFactory getInstance() {
       if (vegtableFactory == null) {
           vegtableFactory= new VegetableFactory();
       }
       return vegtableFactory;
   }

   public Beet getBeet(Type type) {
       if (type == Type.BEET) {
           return new Beet("pink", 5, "Beet") {
               @Override
               boolean isRipe() {
                   throw new UnsupportedOperationException("Not supported yet.");
}
           }
       } else if (type == Type.CARROT) {
           return new Carrot("red", 3, "Carrot");
       }
       return null;
   }
}
